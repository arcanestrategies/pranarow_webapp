<?php

use Illuminate\Database\Seeder;

class SetupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createRoles();
        $this->createFoci();
        $this->createWorkoutLevels();
        $this->createUnits();
    }

    private function createUnits()
    {
        DB::table('units')->insert([
            [
                'id' => 1,
                'name' => 'Foot',
                'unit' => 'ft',
                'precision' => 5280
            ],
            [
                'id' => 2,
                'name' => 'Metre',
                'unit' => 'm',
                'precision' => 1609.34
            ],
            [
                'id' => 3,
                'name' => 'Mile',
                'unit' => 'mi',
                'precision' => 1
            ],
            [
                'id' => 4,
                'name' => 'Kilometre',
                'unit' => 'km',
                'precision' => 1.61
            ],
        ]);
    }

    private function createWorkoutLevels()
    {
        DB::table('workout_levels')->insert([
            [
                'id' => 1,
                'name' => 'Light',
            ],
            [
                'id' => 2,
                'name' => 'Medium',
            ],
            [
                'id' => 3,
                'name' => 'Heavy',
            ],
        ]);
    }

    private function createFoci()
    {

        DB::table('foci')->insert([
            [
                'id' => 1,
                'name' => 'Strength Training',
            ],
            [
                'id' => 2,
                'name' => 'Learn To Row',
            ],
            [
                'id' => 3,
                'name' => 'Method',
            ],
            [
                'id' => 9,
                'name' => 'Training',
            ],
        ]);
    }

    private function createRoles()
    {

        DB::table('roles')->insert([
            [
                'id' => 1,
                'name' => 'Basic User',
            ],
            [
                'id' => 2,
                'name' => 'Coach',
            ],
            [
                'id' => 3,
                'name' => 'Instructor',
            ],
            [
                'id' => 9,
                'name' => 'Admin',
            ],
        ]);
    }
}
