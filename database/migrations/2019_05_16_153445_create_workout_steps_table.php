<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkoutStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workout_steps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('workout_id');
            $table->integer('level_id');
            $table->integer('unit_id');
            $table->integer('sort')->default(1);
            $table->string('name')->nullable();
            $table->integer('duration')->nullable();
            $table->integer('distance')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workout_steps');
    }
}
