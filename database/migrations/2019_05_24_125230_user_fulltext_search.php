<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UserFulltextSearch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $fields = [
            'username',
            'first_name',
            'last_name',
            'email',
        ];
        // Full Text Index
        DB::statement('ALTER TABLE users ADD FULLTEXT fulltext_index (' . implode(', ', $fields) . ')');
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
