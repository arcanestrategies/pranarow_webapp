<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', (isset($title)) ? $title : __('Home') ) | {{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700&display=swap" rel="stylesheet"
        type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
        integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>

    <div class="d-flex toggled" id="wrapper">

        <!-- Sidebar -->
        <div class="bg-light border-right" id="sidebar-wrapper">
            {{-- <div class="sidebar-heading">Start Bootstrap </div> --}}
            <div class="list-group list-group-flush">
                @yield('sidebar')
            </div>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="bg-white">
                <div class="container py-md-3">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a href="{{ url('/') }}" title="{{ config('app.name', 'Prana Row') }}" class="navbar-brand">
                            <img src="{{ asset('img/logo.png') }}" alt="{{ config('app.name', 'Prana Row') }}">
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                                @foreach (\App\Helpers\Nav::getTopMenu() as $item)
                                <li class="nav-item {{ (isset($item['children']))?'dropdown':'' }}">

                                    @if (isset($item['children']))
                                    <a class="nav-link dropdown-toggle {{ Request::is($item['url'])?'active':'' }}"
                                         href="{{ url($item['url']) }}" role="button"
                                        aria-haspopup="true" aria-expanded="false">
                                        {{ $item['title'] }}
                                    </a>
                                    <div class="dropdown-menu">
                                        @foreach ($item['children'] as $child)
                                        <a class="dropdown-item" href="{{$child['url']}}">{{ $child['title'] }}</a>
                                        @endforeach
                                    </div>
                                    @else
                                    <a class="nav-link {{ Request::is($item['url'])?'active':'' }}"
                                        href="{{ url($item['url']) }}">
                                        {{ $item['title'] }}
                                    </a>
                                    @endif


                                </li>
                                @endforeach
                            </ul>
                            <div class="display-inline my-2 my-lg-0 top-right-menu">
                                <a href="{{ url('search') }}" title="{{ __('Search') }}" class="mx-3 mx-sm-2">
                                    <i class="fa fa-search fa-2x"></i>
                                </a>
                                <a href="{{ url('cart') }}" title="{{ __('Shopping Cart') }}" class="mx-3 mx-sm-2">
                                    <i class="fa fa-shopping-cart fa-2x"></i>
                                </a>
                                <a href="{{ url('profile') }}" title="{{ __('Profile') }}" class="mx-3 mx-sm-2">
                                    <i class="fa fa-user fa-2x"></i>
                                </a>
                                <span class="mx-3 mx-sm-2"></span>
                                @guest
                                <a href="{{ url('login') }}">
                                    Log In
                                </a>
                                /
                                <a href="{{ url('register') }}">
                                    Sign Up
                                </a>
                                @else
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">
                                    @csrf
                                </form>
                                @endguest
                            </div>
                        </div>
                    </nav>
                </div>

            </div>

            @if (Request::is('/'))
            @yield('content')

            @else
            <main class="py-4">
                <div class="container">
                    @yield('content')
                </div>
            </main>
            @endif
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <div class="footer bg-white py-md-5 py-2">
        <div class="container">
            <div class="row">
                <div class="col-md-3  text-center text-md-left">
                    <a href="{{ url('/') }}" title="{{ config('app.name', 'Prana Row') }}" class="navbar-brand">
                        <img src="{{ asset('img/logo.png') }}" alt="{{ config('app.name', 'Prana Row') }}">
                    </a>
                </div>
                <div class="col-md-6 py-2 py-md-0">
                    <div class="row">
                        <div class="col-md-4">
                            <h4>{{__('About')}}</h4>
                            @foreach (\App\Helpers\Nav::getFooterAboutMenu() as $item)
                            <a href="{{ $item['url'] }}" title="{{ $item['title'] }}" class="d-block text-secondary">
                                {{ $item['title'] }}
                            </a>
                            @endforeach
                        </div>
                        <div class="col-md-4">
                            <h4>{{__('User')}}</h4>
                            @foreach (\App\Helpers\Nav::getFooterUserMenu() as $item)
                            <a href="{{ $item['url'] }}" title="{{ $item['title'] }}" class="d-block text-secondary">
                                {{ $item['title'] }}
                            </a>
                            @endforeach
                        </div>
                        <div class="col-md-4">
                            <h4>{{__('Shopping')}}</h4>
                            @foreach (\App\Helpers\Nav::getFooterShoppingMenu() as $item)
                            <a href="{{ $item['url'] }}" title="{{ $item['title'] }}" class="d-block text-secondary">
                                {{ $item['title'] }}
                            </a>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="d-flex justify-content-center justify-content-md-end">
                        @foreach (\App\Helpers\Nav::getSocialMenu() as $item)
                        <a href="{{ $item['url'] }}" title="{{ $item['title'] }}" class="text-secondary ml-3">
                            <i class="{{ $item['icon'] }}"></i>
                        </a>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="bg-white">
        <div class="container text-sm text-muted text-center pb-3">
            Copyright © 2019 Prana Row. All rights reserved.
            <a href="{{ url('terms') }}">
                {{__('Terms and Conditions')}}
            </a>
            <a href="{{ url('privacy') }}">
                {{__('Privacy Policy')}}
            </a>

        </div>
    </div>
</body>

</html>