@if ($users->total() && $users->lastPage() > 1 )
{{__('Total')}}: {{$users->total()}}
@php
    for ($i=1; $i <= $users->lastPage(); $i++) { 
        $pages[$i]=$i;
    }
@endphp
@include('modules.form.field',[
    'col'=>true,
    'label'=>'Page',
    'type'=>'select',
    'name'=>'page',
    'options'=>$pages,
    'attr'=>[
        'data-change'=>'profileSearchPageChange'
    ],  
    'value'=>$users->currentPage()
])
@endif
@foreach ($users as $user)
<div class="form-check">
    <input class="form-check-input" name="users[]" type="checkbox" value="{{ $user->id }}" id="defaultCheck1">
    <label class="form-check-label" for="defaultCheck1">
        {{ $user->full_name }}
    </label>
</div>
@endforeach