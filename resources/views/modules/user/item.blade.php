<div class="col-6 col-lg-3 mb-3 user_item_container" id="user_{{ $item->id }}">
    <div class="card ">
        <a href="{{ $user->profile_url }}">
            <img class="card-img-top" src="{{ $user->image_path }}" alt="{{ $user->full_name }}">
        </a>
        <div class="card-footer">
            {{ $user->full_name }}
            <div class="small font-italic">
                @if ($item->isOwner($user->id))
                {{ __('Owner') }}
                @elseif ($item->isCoach($user->id))
                {{ __('Coach') }}
                @else
                {{ __('Member') }}
                @endif
            </div>

            <div>
                <small>
                    {{ $user->role->name }}
                </small>
            </div>

        </div>

        @if (!isset($readonly))
        <div class="card-footer ">
            {{-- <form action=""> --}}
            <ul class="nav nav-pills">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle active" data-toggle="dropdown" href="#" role="button"
                        aria-haspopup="true" aria-expanded="false">Action</a>
                    <div class="dropdown-menu">
                        @if ($item->isOwner($user->id))
                        <a href="#" class="dropdown-item" data-action="groupUserDemote"
                            data-user_id={{ $user->id }}>{{ __('Make Member') }}</a>
                        @else
                        <a href="#" class="dropdown-item" data-action="groupUserPromote"
                            data-user_id={{ $user->id }}>{{ __('Make Owner') }}</a>
                        @endif
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item" data-user_id={{ $user->id }}
                            data-action="groupUserRemove">{{ __('Remove Member') }}</a>
                    </div>
                </li>
            </ul>
            {{-- </form> --}}
        </div>
        @endif

        @if (Auth::user()->id == $user->id)
        <div class="card-footer ">
            {{-- <form action=""> --}}
            <a href="#" class="btn btn-sm btn-danger" data-action="groupUserLeave">{{ __('Leave Group') }}</a>
            {{-- </form> --}}
        </div>
        @endif
    </div>
</div>