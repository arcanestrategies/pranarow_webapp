{{-- 
Required
$name
 --}}

@php

    // if(isset($options)){
    //     dd($options);
    // }
    $id = $id??$name;
    $class = $class??'';
    
    $type = $type??'text';
    $base_class = 'form-control';    
    switch ($type) {
        case 'checkbox':
        case 'radio':
            $base_class = 'form-control';
            break;
        case 'file':
        case 'image':
            $base_class = 'form-control-file';
            break;
        
        default:
            # code...
            break;
    }
    $class = $base_class.' '.$class;
    $essential = "name=$name id=$id class=$class";

    if(empty($value)){
        if(!empty($item)){
            $value = $item->$id;
            if(isset($item->$id->path) && $type == 'image'){
                $value = $item->$id->path;
            }
        }
    }
    $value = $value??'';

    $attr_string = '';
    if(!empty($attr)){
        foreach ($attr as $k => $v) {
            $attr_string = $attr_string.' '.$k.'='.$v.' ';
        }
    }

    $essential = $essential.' '.$attr_string;

    if(isset($readonly)){
        $essential = $essential.' readonly';
    }

@endphp

<div class="form-group {{ (!empty($col))?'row':'' }}">
    @if (!empty($label))
        <label for="{{ $name }}" class="{{ (!empty($col))?'col-sm-4 col-form-label text-md-right':'' }}">{{ $label }}</label>
    @else
        @if(!empty($col))
        <div class="col-sm-4"></div>
        @endif
    @endif

    @if (!empty($col))
        <div class="col-sm-8 {{ isset($readonly)?'py-2 font-weight-bold':'' }}">
    @endif

        @switch($type)

            @case('text')
            @case('email')
            @case('number')
            @case('password')
            @case('date')
                @if (isset($readonly))
                    {{ $value }}
                @else
                    <input
                        {{ $essential }}
                        type="{{ $type }}" 
                        value="{{ $value }}"
                        {{ (!empty($help))?'aria-describedby="'.$name.'Help"':'' }} 
                        {{ (!empty($placeholder))?'placeholder="'.$placeholder.'"':'' }} 
                    >
                @endif
                @break
            @case('textarea')
                @if (isset($readonly))
                    {{ $value }}
                @else
                    <textarea {{ $essential }} >{{ $value }}</textarea>
                @endif
                @break
            @case('select')
                @if (isset($readonly))
                    {{ $options[$value] }}
                @else
                    <select {{ $essential }} >
                        @foreach ($options as $k => $v)
                            <option value="{{ $k }}" {{ ($k == $value)?'selected':'' }}>{{ $v }}</option>
                        @endforeach
                    </select>
                @endif
                @break
            @case('checkbox')
            @case('radio')
                @foreach ($options as $k => $v)
                    <div class="form-check" >
                        <input class="form-check-input" name="{{ $name }}" type="{{ $type }}" value="{{ $k }}" id="{{$id}}_{{$k}}" {{ ($value == $k)?'checked':'' }}>
                        <label class="form-check-label" for="{{$id}}_{{$k}}">
                            {{ $v }}
                        </label>
                    </div>
                @endforeach
                @break
            @case('image')
                @if (!empty($value))
                <div class="row justify-content-center py-2">
                    <div class="col-4">
                        <img class="img-thumbnail" src="{{ asset('storage/'.$value) }}">
                    </div>
                </div>
                    
                @endif
                @if (!isset($readonly))
                    <input {{ $essential }} type="file" value="{{ $value }}">
                @endif
                @break
            @default
                
        @endswitch

    @if (!empty($col))
        </div>
    @endif
    @if (!empty($help))
        <small id="{{ $name.'Help' }}" class="form-text text-muted">{!! $help !!}</small>
    @endif

</div>