<div class="card border-0 rounded-0">
    <img class="card-img border-0 rounded-0" src="{{ asset($card_img) }}" alt="{{ $card_title }}">
    <div class="card-img-overlay d-flex align-items-center">
        <div class="container">

            <div class="{{ (isset($align)&&$align=='right')?'text-right':'' }}">
                <div class="row">
                    <div class="col-md-6 {{ (isset($align)&&$align=='right')?'ml-auto':'' }}">
                        <h1 class="display-4 {{ (isset($align)&&$align=='right')?'text-primary':'' }}">
                            {{ $card_title }}
                        </h1>
                        <p class="card-text {{ (isset($align)&&$align=='right')?'text-white':'' }}">{{ $card_text }}</p>
                        @if (isset($card_btn))
                        <a href="{{ $card_url }}" class="btn btn-primary">{{ $card_btn }}</a>
                        @endif

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>