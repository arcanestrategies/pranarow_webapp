<div class="icon">
    <div class="icon-circle">
        <i class="fa fa-4x {{ $icon_icon }}"></i>
    </div>
    <div class="icon-text">
        {{$icon_text}}
    </div>
</div>