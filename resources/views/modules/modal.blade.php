<div class="modal fade" id="{{ $modal->id }}Modal" tabindex="-1" role="dialog"
    aria-labelledby="{{ $modal->id }}ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                @if ($modal->title)
                <h5 class="modal-title" id="{{ $modal->id }}ModalLabel">{{ $modal->title }}</h5>
                @endif
                <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('Close') }}">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @includeIf($modal->view, ['data' => $modal->data])
            </div>
            {{-- <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div> --}}
        </div>
    </div>
</div>