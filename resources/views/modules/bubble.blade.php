<div class="bubble">
    <a class="bubble_link" href="{{ $bubble_url }}" target="_blank" title="{{ $bubble_title }}">
        <img src="{{ $bubble_img }}" alt="{{ $bubble_title }}" class="">
    </a>
    @if (!empty($bubble_data))
        <a class="bubble_action" href="#" {{$bubble_data}}><i class="fa fa-times"></i></a>
    @endif
</div>