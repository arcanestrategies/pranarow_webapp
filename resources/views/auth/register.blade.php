@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        {{-- @include('modules.form.field',[
                            'col'=>true,
                            'label'=>__('Name'),
                            'name'=>'name'
                        ]) --}}
                        @include('modules.form.field',[
                            'col'=>true,
                            'label'=>__('Username'),
                            'name'=>'username'
                        ])
                        @include('modules.form.field',[
                            'col'=>true,
                            'label'=>__('Email'),
                            'name'=>'email'
                        ])
                        @include('modules.form.field',[
                            'col'=>true,
                            'label'=>__('Password'),
                            'name'=>'password',
                            'type'=>'password'
                        ])
                        @include('modules.form.field',[
                            'col'=>true,
                            'label'=>__('Confirm Password'),
                            'name'=>'password_confirmation',
                            'type'=>'password'
                        ])
                        
                        @include('modules.response')

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <a href="#" class="btn btn-primary" data-action="register">
                                    {{ __('Register') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
