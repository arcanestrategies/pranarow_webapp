@extends('layouts.app')

@section('content')
<h1>{{$title}}</h1>
<div>
    <form action="">
        @include('modules.response')
        @include('modules.form.field',[
        'name'=>'image',
        'label'=>'Image',
        'type'=>'image'
        ])
        @include('modules.form.field',[
        'name'=>'username',
        'label'=>'Username',
        ])
        @include('modules.form.field',[
        'label'=>__('Email'),
        'name'=>'email'
        ])
        @include('modules.form.field',[
        'label'=>__('First Name'),
        'name'=>'first_name'
        ])
        @include('modules.form.field',[
        'label'=>__('Last Name'),
        'name'=>'last_name'
        ])
        @include('modules.form.field',[
        'label'=>__('Phone Number'),
        'name'=>'phone'
        ])

        @include('modules.form.field',[
        'label'=>__('Birthday'),
        'type'=>'date',
        'name'=>'birth_date',
        'value'=>(isset($item) && isset($item->profile))?$item->profile->birth_date:null
        ])
        @include('modules.form.field',[
        'label'=>__('Pin Code'),
        'type'=>'number',
        'name'=>'pin'
        ])
        <div class="text-center">
            <a href="#" data-action="profileStore" class="btn btn-primary">{{ __('Update') }}</a>
            <a href="#" data-action="profileUpgrade" class="btn btn-primary">{{ __('Upgrade Subscription') }}</a>
            <a href="#" data-action="profileDegrade" class="btn btn-primary">{{ __('Degrade Subscription') }}</a>
        </div>
    </form>
</div>
@endsection