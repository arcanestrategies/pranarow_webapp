@extends('layouts.app')

@section('content')
@if (Auth::user()->id == $item->id)
<a href="{{ url('profile/edit') }}" class="btn btn-sm btn-primary">{{ __('Edit Profile') }}</a>
@endif
<div class="text-center">
    <h1 class="text-center">{{$title}}</h1>
    <small>{{ $item->role->name }}</small>
</div>
<div class="row">
    <div class="col-md-4 my-2 text-right">
        <img class="img-thumbnail" src="{{ $item->image_path }}" alt="{{ $item->full_name }}">
    </div>
    <div class="col-md-8 my-2">
        <h5>
            {{ __('Workouts') }}
        </h5>
        <div>
            @forelse ($item->workouts as $workout)
            <div>
                <a href="{{$workout->view_url}}">{{ $workout->name }}</a> {{ __("Workout Duration") }}: {{ $workout->total_duration }}{{ __("min") }} {{ __("Workout Steps") }}: {{ $workout->total_steps }}
            </div>
            @empty
            <div class="alert alert-info">
                {{ __("Haven't created any workout") }}
            </div>
            @endforelse
        </div>
        <hr>
        <h5>
            {{ __('Groups') }}
        </h5>
        <div>
            @forelse ($item->groups as $group)
            <div>
                <a href="{{$group->view_url}}">{{ $group->name }}</a> {{ __("Group Members") }}: {{ $group->total_members }}
            </div>
            @empty
            <div class="alert alert-info">
                {{ __("Haven't started any group") }}
            </div>
            @endforelse
        </div>
        <hr>
        <h5>
            {{ __('Subscriptions') }}
            <small>{{ $item->role->name }}</small>
        </h5>
        <hr>
        <h5>
            {{ __('Devices') }}
        </h5>
        <div>
            @forelse ($item->devices as $device)
            <div class="my-2 device_container">
                <form action="">
                    <input type="hidden" name="device_id" value="{{$device->id}}">
                    {{ $device->name }} | {{__('Serial')}}: {{ $device->serial }} | {{__('Registered At')}} {{$device->created_at->toFormattedDateString()}} 
                    <a href="#" class="btn btn-danger btn-sm" data-action="deviceRemove">{{__('Remove Device')}}</a>
                    @include('modules.response')
                </form>
            </div>
            @empty
            <div class="alert alert-info">
                {{ __("Haven't registered any device yet") }}
            </div>
            @endforelse
        </div>

    </div>
</div>
@endsection