@extends('layouts.app')

@section('sidebar')
@include('workout.sidebar')
@endsection

@section('content')
<h1>{{$title}}</h1>
<div class="row justify-content-center">
    <div class="col-md-8">
        <form action="">
            @if (isset($item))
                <input type="hidden" name="id" value="{{ $item->id }}">
                <input type="hidden" name="workout_id" value="{{ $item->id }}">

                <h4>{{ __("Sharing is Caring") }}</h4>                
                <ul class="nav nav-tabs" id="sharedTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="shared-user-tab" data-toggle="tab" href="#shared-user" role="tab" aria-controls="shared-user"
                            aria-selected="true">{{ __("Users") }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="shared-group-tab" data-toggle="tab" href="#shared-group" role="tab" aria-controls="shared-group"
                            aria-selected="false">{{ __("Groups") }}</a>
                    </li>
                </ul>

                <div class="tab-content py-3" id="sharedTabContent">
                    <div class="tab-pane fade show active" id="shared-user" role="tabpanel" aria-labelledby="shared-user-tab">
                        <div class="d-flex justify-content-center">
                            @forelse ($item->users as $user)
                                @include('modules.bubble',[
                                    'bubble_title'=>$user->full_name,
                                    'bubble_url'=>$user->profile_url,
                                    'bubble_img'=>$user->image_path,
                                    'bubble_data'=>'data-action=workoutUserRemove data-user_id='.$user->id.'',
                                ])
                            @empty
                                <div class="alert alert-info">
                                    {{ __("You haven't shared this workout with any user yet") }}
                                </div>
                            @endforelse
                        </div>
                    </div>
                    <div class="tab-pane fade" id="shared-group" role="tabpanel" aria-labelledby="shared-group-tab">
                        <div class="d-flex justify-content-center">
                            @forelse ($item->groups as $group)
                                @include('modules.bubble',[
                                    'bubble_title'=>$group->name,
                                    'bubble_url'=>$group->view_url,
                                    'bubble_img'=>$group->image_path,
                                    'bubble_data'=>'data-action=workoutGroupRemove data-group_id='.$group->id.'',
                                ])
                            @empty
                                <div class="alert alert-info">
                                    {{ __("You haven't shared this workout with any group yet") }}
                                </div>
                            @endforelse
                        </div>
                    </div>
                </div>

                @if (!isset($readonly))
                    @php
                    $modal = new App\Helpers\Modal('shareWorkout',__('Share Workout'),'workout.modal.user.add',['workout_id'=>$item->id]);
                    @endphp
                    <div class="text-center my-2">
                        <a href="#" data-action="modalCreate" data-modal="{{ $modal->toJson() }}"
                            class="btn btn-primary">{{ __('Share Workout') }}</a>
                    </div>
                @endif
                <hr>
                
            @endif
            @include('modules.form.field',[
            'name'=>'image',
            'label'=>'Image',
            'type'=>'image',
            'col'=>true,
            ])
            @include('modules.form.field',[
            'name'=>'focus_id',
            'label'=>'Focus',
            'type'=>'select',
            'options'=>App\Focus::pluck('name','id')->toArray(),
            'col'=>true,
            ])
            @include('modules.form.field',[
            'name'=>'name',
            'label'=>'Name',
            'col'=>true,
            ])
            @include('modules.form.field',[
            'name'=>'description',
            'label'=>'Description',
            'type'=>'textarea',
            'col'=>true,
            ])
            @if (isset($item))
                @if (!isset($readonly))
                    @php
                    $modal = new App\Helpers\Modal('addStep',__('Add Step'),'workout.modal.step.add',['workout_id'=>$item->id]);
                    @endphp
                    <div class="text-center">
                        <a href="#" data-action="modalCreate" data-modal="{{ $modal->toJson() }}"
                            class="btn btn-primary">{{ __('Add Step') }}</a>
                    </div>
                @endif

                <div class="workout-steps-container">
                    @include('workoutstep.steps')
                </div>

            @endif
            @if (!isset($readonly))
            <div class="text-center">
                @include('modules.response')
                <a href="#" class="btn btn-primary"
                    data-action="workoutStore">{{ (isset($item))?__('Update'):__('Save') }}</a>
            </div>
            @endif
        </form>
    </div>
</div>
@endsection