@extends('layouts.app')

@section('sidebar')
    @include('workout.sidebar')
@endsection

@section('content')
<h1>{{$title}}</h1>

@if ($user->workouts->count() > 0)
<div class="card-columns">
    @foreach ($user->workouts as $item)
    <div class="card">

        @if ($item->image_path)
        <a href="{{ $item->view_url }}">
            <img class="card-img-top" src="{{ $item->image_path }}" alt="{{ $item->name }}">
        </a>
        @endif


        <div class="card-body">
            <h5 class="card-title">
                {{ $item->name }}
            </h5>
            @if ($item->total_duration)
            <h6 class="card-subtitle">
                {{ __('Total Duration') }}: {{ $item->total_duration }} {{ __('minute(s)') }} <br>
                {{ __('Total Steps') }}: {{ $item->total_steps }} {{ __('step(s)') }} <br>
                {{ __('Created At') }}: {{$item->created_at->toFormattedDateString()}} <br>
                {{ __('Updated At') }}: {{$item->updated_at->toFormattedDateString()}} <br>
            </h6>
            @endif
            @if ($item->description)
            <p class="card-text">
                {{ $item->description }}
            </p>
            @endif
        </div>
        <div class="card-footer clearfix">
            @if (Auth::user()->id == $item->user_id)
            <a href="{{ $item->url }}" class="card-link float-right btn btn-primary">{{__('Edit')}}</a>
            @endif
        </div>

    </div>
    @endforeach
</div>
@else
    <div class="alert alert-info">
        {{__('Create your first workout using button above')}}
    </div>
@endif
@endsection