@php
if(isset($data) && isset($data['step_id'])){
$item = App\WorkoutStep::find($data['step_id']);
}
@endphp
<form action="">
    <input type="hidden" name="workout_id" value="{{ $data['workout_id'] }}">
    @if (isset($item))
    <input type="hidden" name="id" value="{{ $item->id }}">
    <input type="hidden" name="sort" value="{{ $item->sort }}">

    @endif
    <div class="row">
        <div class="col-md-6">
            @include('modules.form.field',[
            'label'=>'Rowing Level',
            'name'=>'level_id',
            'type'=>'radio',
            'options'=>App\WorkoutLevel::pluck('name','id')->toArray(),
            ])
        </div>
        <div class="col-md-6">
            @include('modules.form.field',[
            'label'=>'Duration',
            'name'=>'duration',
            'type'=>'number',
            'help'=>'in minutes'
            ])
        </div>

        <div class="col-md-6">
            @include('modules.form.field',[
            'label'=>'Measurement Unit',
            'name'=>'unit_id',
            'type'=>'radio',
            'options'=>App\Unit::pluck('name','id')->toArray(),
            ])
        </div>
        <div class="col-md-6">
            @include('modules.form.field',[
            'label'=>'Distance',
            'name'=>'distance',
            'type'=>'number',
            ])
        </div>

    </div>
    @include('modules.form.field',[
    'label'=>'Step Name',
    'name'=>'name',
    'help'=>'Optional'
    ])




    <div class="text-center">
        <a href="#" data-action="workoutStepStore" class="btn btn-primary">{{(isset($item))?__('Update'):__('Add')}}</a>
    </div>
</form>