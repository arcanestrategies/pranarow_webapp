<ul class="nav nav-tabs" id="shareTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="user-tab" data-toggle="tab" href="#user" role="tab" aria-controls="user"
            aria-selected="true">{{ __("Users") }}</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="group-tab" data-toggle="tab" href="#group" role="tab" aria-controls="group"
            aria-selected="false">{{ __("Groups") }}</a>
    </li>
</ul>
<div class="tab-content py-3" id="shareTabContent">
    <div class="tab-pane fade show active" id="user" role="tabpanel" aria-labelledby="user-tab">
        <form action="">
            <input type="hidden" name="workout_id" value="{{ $data['workout_id'] }}">
            <input type="hidden" name="per_page" value="5">
            <div class="row">
                <div class="col-md-12">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" name="search" placeholder="{{ __('Search User') }}"
                            aria-label="{{ __('Search User') }}" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <a href="#" class="btn btn-outline-primary"
                                data-action="profileSearch">{{ __('Search') }}</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="user-list-container">

            </div>
            @include('modules.response')
            <div class="text-center">
                <a href="#" data-action="workoutUserAdd" class="btn btn-primary">{{ __('Share') }}</a>
            </div>
        </form>
    </div>
    <div class="tab-pane fade" id="group" role="tabpanel" aria-labelledby="group-tab">
        <form action="">
            <input type="hidden" name="workout_id" value="{{ $data['workout_id'] }}">
            <input type="hidden" name="per_page" value="5">
            <div class="group-list-container">
                @forelse (Auth::user()->groups as $group)
                    <div class="form-check">
                        <input class="form-check-input" name="groups[]" type="checkbox" value="{{ $group->id }}" id="groupsCheck">
                        <label class="form-check-label" for="groupsCheck">
                            {{ $group->name }}
                        </label>
                    </div>
                @empty
                <div class="alert alert-info">
                    {{ __("You don't have any groups yet") }}
                </div>
                @endforelse
            </div>
            @include('modules.response')
            <div class="text-center">
                <a href="#" data-action="workoutGroupAdd" class="btn btn-primary">{{ __('Share') }}</a>
            </div>
        </form>
    </div>
</div>