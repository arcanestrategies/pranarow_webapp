@extends('layouts.app')

@section('title')
{{__('Sorry Page Not Found')}}
@endsection

@section('content')
<div>
    <h1>
        {{__('Sorry Page Not Found')}}
    </h1>
    <h2>{{ $exception->getMessage() }}</h2>
    <p>
        {{__("The page you're looking for not found please use search button above")}}
    </p>
</div>
@endsection