@extends('layouts.app')

@section('title')
{{__('Sorry Access Denied')}}
@endsection

@section('content')
<div>
    <h1>
        {{__('Sorry Access Denied')}}
    </h1>
    <h2>{{ $exception->getMessage() }}</h2>
    <p>
        {{__("You don't have access to this item")}}
    </p>
</div>
@endsection