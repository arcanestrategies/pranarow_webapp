<div class="workout-steps {{(!isset($readonly))?'sortable':''}}" data-workout_id="{{ $item->id }}">
    @if ($item->steps)
        <div class="alert alert-success mt-4">
            {{ __('Total Duration') }}: {{ $item->total_duration }} {{ __('Minute(s)') }}
        </div>
        @foreach ($item->steps as $step)
            @include('workoutstep.step',['step'=>$step])
        @endforeach
    @else
        <div class="alert alert-warning my-2">
            {{ __('Please add steps') }}
        </div>
    @endif
</div>