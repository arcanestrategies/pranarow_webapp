<div class="card card-info my-2 {{ $step->level->card_class }}" data-id="{{ $step->id }}">
    @if ($step->name)
    <div class="card-header">
        {{$step->name}}
    </div>
    @endif
    <div class="card-body">
        {{__('Level')}}: {{$step->level->name}} |
        {{__('Duration')}}: {{$step->duration}} {{__('minute(s)')}} |
        {{__('Distance')}}: {{$step->distance}} {{$step->unit->name}}
    </div>
    @if (Auth::user()->id == $item->user_id && !isset($readonly))
    <div class="card-footer text-right small">
        <a href="#" data-action="workoutStepRemove" class="btn btn-danger btn-sm">{{ __('Remove Step') }}</a>
        @php
        $modal = new App\Helpers\Modal('addStep',__('Edit
        Step'),'workout.modal.step.add',['workout_id'=>$item->id,'step_id'=>$step->id]);
        @endphp
        <a href="#" data-action="modalCreate" data-modal="{{ $modal->toJson() }}"
            class="btn btn-warning btn-sm">{{ __('Edit Step') }}</a>
    </div>
    @endif
</div>