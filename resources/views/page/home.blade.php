@extends('layouts.app')

@section('content')
<div class="">
    <div class="homepage-hero">

        <!--@include('modules.hero',[
        'card_img'=>'img/home/man-carrying-barbel.jpg',
        'card_title'=>'Card Title',
        'card_text'=>'This is a wider card with supporting text below as
        a natural lead-in to additional content. This content is a little bit longer.',
        'card_btn'=>'Learn More',
        'card_url'=>'#',
        ])-->
        @include('modules.hero',[
        'align'=>'right',
        'card_img'=>'img/home/background.jpg',
        'card_title'=>'Card Title',
        'card_text'=>'This is a wider card with supporting text below as
        a natural lead-in to additional content. This content is a little bit longer.',
        'card_btn'=>'Learn More',
        'card_url'=>'#',
        ])

    </div>

</div>
<div class="bg-secondary">
    <div class="homepage-icon text-center d-flex align-items-center justify-content-center">
        @include('modules.icon',[
        'icon_text'=>__('Health'),
        'icon_icon'=>'fa-heartbeat'
        ])
        @include('modules.icon',[
        'icon_text'=>__('Timing'),
        'icon_icon'=>'fa-clock'
        ])
        @include('modules.icon',[
        'icon_text'=>__('Energy'),
        'icon_icon'=>'fa-bolt'
        ])
        @include('modules.icon',[
        'icon_text'=>__('Strength'),
        'icon_icon'=>'fa-dumbbell'
        ])
    </div>
</div>
<!--div class="map" style="background-image:url({{asset('img/home/map.jpg')}})">
    <div class="map-search-container">
        <div class="container h-100 d-flex justify-content-center align-items-center">
            <div class="row w-100 justify-content-center">
                <div class="col-4">
                    <input type="text" placeholder="{{ __('enter location') }}"
                        class="form-control rounded mr-4 bg-secondary text-white">
                </div>
                <div class="col-1">
                    <a href="#" class="rounded btn btn-primary w-100">{{ __('go') }}</a>
                </div>
            </div>
        </div>
    </div>
</div-->
<div class="bg-light">
    <div class="container">

        <div class="d-flex justify-content-center align-items-center py-5">
            <h3 class="m-0 p-0">Exercise</h3>
            <div class="m-2"></div>
            <a href="#" class="btn btn-primary">Learn More</a>

        </div>
    </div>
</div>
@endsection