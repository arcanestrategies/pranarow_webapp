@extends('layouts.app')

@section('sidebar')
    @include('group.sidebar')
@endsection

@section('content')
<h1>{{$title}}</h1>
<div class="row justify-content-center">
    <div class="col-md-8">
        <form action="">
            @if (isset($item))
                <input type="hidden" name="id" value="{{ $item->id }}">
                <input type="hidden" name="group_id" value="{{ $item->id }}">

                <h4>{{__("Shared Workouts")}}</h4>
                @forelse ($item->workouts as $workout)
                    <a href="{{ $workout->view_url }}" target="_blank" class="user_avatar" title="{{ $workout->name }}">
                        <img src="{{ $workout->image_path }}" alt="{{ $workout->name }}" class="">
                    </a>
                @empty
                    <div class="alert alert-info">
                        {{__("Nobody shared any workout to group")}}
                    </div>
                @endforelse
            @endif
            @include('modules.form.field',[
                'name'=>'image',
                'label'=>'Image',
                'type'=>'image',
                'col'=>true,
            ])
            @include('modules.form.field',[
                'name'=>'name',
                'label'=>'Name',
                'col'=>true,
            ])
            @include('modules.form.field',[
                'name'=>'description',
                'label'=>'Description',
                'type'=>'textarea',
                'col'=>true,
            ])
            @if (isset($item))
                @if (!isset($readonly))
                    @php
                        $modal = new App\Helpers\Modal('groupUserAdd',__('Add User'),'group.modal.user.add',['group_id'=>$item->id]);
                    @endphp
                    <div class="text-center mb-3">
                        <a href="#" data-action="modalCreate" data-modal="{{ $modal->toJson() }}" class="btn btn-primary">{{ __('Add User') }}</a>
                    </div>
                @endif
                <div class="group-users-container">
                    @include('modules.user.list',['users'=>$item->users])
                </div>
            @endif
            @if (!isset($readonly))
                <div class="text-center">
                    @include('modules.response')
                    <a href="#" class="btn btn-primary" data-action="groupStore">{{ (isset($item))?__('Update'):__('Save') }}</a>
                </div>
            @endif
        </form>
    </div>
</div>
@endsection
