@extends('layouts.app')

@section('sidebar')
@include('group.sidebar')
@endsection

@section('content')
<h1>{{$title}}</h1>
@if ($user->groups->count() > 0)
<div class="card-columns">
    @foreach ($user->groups as $item)
    <div class="card">

        @if ($item->image_path)
        <a href="{{ $item->view_url }}">
            <img class="card-img-top" src="{{ $item->image_path }}" alt="{{ $item->name }}">
        </a>
        @endif

        <div class="card-body">
            <a href="{{ $item->view_url }}">
                <h5 class="card-title">
                    {{ $item->name }}
                </h5>
            </a>
            @if ($item->total_duration)
            <h6 class="card-subtitle">
                {{ __('Total Members') }}: {{ $item->total_members }} {{ __('minute(s)') }} <br>
                {{ __('Group Leader') }}: {{ $item->leader }} {{ __('minute(s)') }} <br>
                {{ __('Workouts') }}: {{ $item->total_workouts }} {{ __('step(s)') }} <br>
                {{ __('Created At') }}: {{$item->created_at->toFormattedDateString()}} <br>
                {{ __('Updated At') }}: {{$item->updated_at->toFormattedDateString()}} <br>
            </h6>
            @endif
            @if ($item->description)
            <p class="card-text">
                {{ $item->description }}
            </p>
            @endif
        </div>
        <div class="card-footer clearfix">
            @if ($item->isOwner())
            <a href="{{ $item->url }}" class="card-link float-right btn btn-primary">{{__('Edit')}}</a>
            @endif
        </div>

    </div>
    @endforeach
</div>
@else
<div class="alert alert-info">
    {{__('Create your first group using button above')}}
</div>
@endif
@endsection