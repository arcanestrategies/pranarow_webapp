<form action="">
    <input type="hidden" name="group_id" value="{{ $data['group_id'] }}">
    <input type="hidden" name="per_page" value="5">
    <div class="row">
        <div class="col-md-12">
            <div class="input-group mb-3">
                <input type="text" class="form-control" name="search" placeholder="{{ __('Search User') }}" aria-label="{{ __('Search User') }}"
                    aria-describedby="basic-addon2">
                <div class="input-group-append">
                    <a href="#" class="btn btn-outline-primary" data-action="profileSearch">{{ __('Search') }}</a>
                </div>
            </div>
        </div>
    </div>
    <div class="user-list-container">

    </div>
    @include('modules.response')
    <div class="text-center">
        <a href="#" data-action="groupUserAdd" class="btn btn-primary">{{ __('Add') }}</a>
    </div>
</form>