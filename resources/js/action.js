jQuery("document").ready(function($) {
    console.log("action loaded");
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        }
    });

    let btn;
    let form;
    let action;
    let formData;
    let response;
    let ajaxUrl;
    let formType;
    let contentType = false;

    const fieldError = {
        show: () => {
            if (typeof response !== "undefined") {
                fieldError.clear();
                $.each(response.errors, function(i, v) {
                    let field = form.find('[name="' + i + '"]');
                    let fieldFeedback =
                        '<div class="invalid-feedback">' +
                        v.join(", ") +
                        "</div>";
                    if (field.length) {
                        field.addClass("is-invalid");
                        if (field.siblings(".input-group-append").length) {
                            field
                                .siblings(".input-group-append")
                                .after(fieldFeedback);
                        } else {
                            field.after(fieldFeedback);
                        }
                    }
                });
            }
        },
        clear: () => {
            $(".invalid-feedback").remove();
            $(".is-invalid").removeClass("is-invalid");
        }
    };

    const showResponse = () => {
        let responseContainer = form.find(".response");
        responseContainer.attr("class", "response").html("");
        if (typeof response !== "undefined") {
            responseContainer.attr("class", "response");
            let containerClass = "my-2 alert alert-";
            if (response.status === true) {
                containerClass += "success";
            } else {
                containerClass += "danger";
            }
            responseContainer.addClass(containerClass).html(response.message);
        }
    };

    const initBtn = () => {
        $(document).on("click", "[data-action]", function(e) {
            btn = $(this);
            action = btn.data("action");
            form = btn.closest("form");
            if (btn.attr("href") === "#") {
                e.preventDefault();
            }
            if (form.length) {
                // formData = form.serialize();
                formData = new FormData(form[0]);
            }
            initAction();
        });
    };

    const initChange = () => {
        $(document).on("change", "[data-change]", function(e) {
            btn = $(this);
            action = btn.data("change");
            form = btn.closest("form");
            if (btn.attr("href") === "#") {
                e.preventDefault();
            }
            if (form.length) {
                // formData = form.serialize();
                formData = new FormData(form[0]);
            }
            initAction();
        });
    };

    const delayedReload = () => {
        if (
            typeof response !== "undefined" &&
            typeof response.url !== "undefined"
        ) {
            setTimeout(() => {
                window.location = response.url;
            }, 2000);
        }
    };

    const delayedRefresh = () => {
        setTimeout(() => {
            location.reload();
        }, 2000);
    };

    const initAction = () => {
        switch (action) {
            case "modalCreate":
                ajaxUrl = "/modal/create";
                contentType = "application/json";
                let modal = btn.data("modal");
                formData = JSON.stringify(modal);
                $("#" + modal.id + "Modal").remove();
                initAjax(function() {
                    $("body").append(response.view);
                    $("#" + modal.id + "Modal").modal("show");

                    // Reload prevent default
                    initPreventDefault();
                });
                break;
            case "workoutStore":
                ajaxUrl = "/workout/store";
                initAjax(() => {
                    showResponse();
                    if (response.status === true) {
                        delayedReload();
                    }
                });
                break;
            case "workoutUserAdd":
                ajaxUrl = "/workout/user/add";
                initAjax(() => {
                    showResponse();
                    if (response.status === true) {
                        delayedRefresh();
                    }
                });
                break;
            case "workoutUserRemove":
                ajaxUrl = "/workout/user/remove";
                formData.append("user_id", btn.data("user_id"));
                initAjax(() => {
                    showResponse();
                    if (response.status === true) {
                        btn.closest('.bubble').hide("fast");
                    }
                });
                break;
            case "workoutGroupAdd":
                ajaxUrl = "/workout/group/add";
                initAjax(() => {
                    showResponse();
                    if (response.status === true) {
                        delayedRefresh();
                    }
                });
                break;
            case "workoutGroupRemove":
                ajaxUrl = "/workout/group/remove";
                formData.append("group_id", btn.data("group_id"));
                initAjax(() => {
                    showResponse();
                    if (response.status === true) {
                        btn.closest('.bubble').hide("fast");
                    }
                });
                break;
            case "workoutStepStore":
                ajaxUrl = "/workout/step/store";
                initAjax(() => {
                    showResponse();
                    if (response.status === true) {
                        $(".workout-steps-container").html(response.view);
                    }
                });
                break;
            case "workoutStepRemove":
                ajaxUrl = "/workout/step/remove";
                let step_id = btn.closest(".card").data("id");
                formData.append("step_id", step_id);
                initAjax(() => {
                    if (response.status === true) {
                        btn.closest(".card").hide("fast");
                    }
                });
                break;
            case "workoutStepEdit":
                ajaxUrl = "/workout/step/edit";
                initAjax(() => {
                    showResponse();
                    if (response.status === true) {
                        $(".workout-steps-container").html(response.view);
                    }
                });
                break;
            case "groupStore":
                ajaxUrl = "/group/store";
                initAjax(() => {
                    showResponse();
                    if (response.status === true) {
                        delayedReload();
                    }
                });
                break;
            case "groupUserAdd":
                ajaxUrl = "/group/user/add";
                initAjax(() => {
                    showResponse();
                    if (response.status === true) {
                        delayedRefresh();
                    }
                });
                break;
            case "groupUserPromote":
                ajaxUrl = "/group/user/promote";
                formData.append("user_id", btn.data("user_id"));
                initAjax(() => {
                    showResponse();
                    if (response.status === true) {
                        setTimeout(() => {
                            btn.closest(".user_item_container").replaceWith(
                                response.view
                            );
                        }, 2000);
                    }
                });
                break;
            case "groupUserDemote":
                ajaxUrl = "/group/user/demote";
                formData.append("user_id", btn.data("user_id"));
                initAjax(() => {
                    showResponse();
                    if (response.status === true) {
                        setTimeout(() => {
                            btn.closest(".user_item_container").replaceWith(
                                response.view
                            );
                        }, 2000);
                    }
                });
                break;
            case "groupUserRemove":
                ajaxUrl = "/group/user/remove";
                formData.append("user_id", btn.data("user_id"));
                initAjax(() => {
                    showResponse();
                    if (response.status === true) {
                        setTimeout(() => {
                            btn.closest(".user_item_container").hide("fast");
                        }, 2000);
                    }
                });
                break;
            case "groupUserLeave":
                ajaxUrl = "/group/user/leave";
                initAjax(() => {
                    showResponse();
                    if (response.status === true) {
                        setTimeout(() => {
                            btn.closest(".user_item_container").hide("fast");
                        }, 2000);
                    }
                });
                break;
            case "deviceRemove":
                ajaxUrl = "/device/remove";
                initAjax(() => {
                    showResponse();
                    if (response.status === true) {
                        setTimeout(() => {
                            btn.closest(".device_container").hide("fast");
                        }, 2000);
                    }
                });
                break;
            case "sidebarToggle":
                $("#wrapper").toggleClass("toggled");
                break;
            case "profileStore":
                ajaxUrl = "/profile/store";
                initAjax(() => {
                    showResponse();
                    if (response.status === true) {
                        delayedRefresh();
                    }
                });
                break;
            case "profileUpgrade":
                ajaxUrl = "/profile/upgrade";
                initAjax(() => {
                    showResponse();
                    if (response.status === true) {
                        delayedReload();
                    }
                });
                break;
            case "profileDegrade":
                ajaxUrl = "/profile/degrade";
                initAjax(() => {
                    showResponse();
                    if (response.status === true) {
                        delayedReload();
                    }
                });
                break;
            case "login":
                ajaxUrl = "/login";
                initAjax(() => {
                    showResponse();
                    if (response.status === true) {
                        delayedReload();
                    }
                });
                break;
            case "register":
                ajaxUrl = "/register";
                initAjax(() => {
                    showResponse();
                    if (response.status === true) {
                        delayedReload();
                    }
                });
                break;
            case "profileSearch":
            case "profileSearchPageChange":
                ajaxUrl = "/profile/search";
                initAjax(() => {
                    showResponse();
                    if (response.status === true) {
                        $(".user-list-container").html(response.view);
                    }
                });
                break;
            default:
                console.log("No action defined for action:");
                console.log(action);
                break;
        }
    };

    const initAjax = callback => {
        btnLoading();
        let ajaxConfig = {
            url: ajaxUrl,
            type: "POST",
            dataType: "JSON",
            data: formData,
            contentType: contentType,
            processData: false
        };

        $.ajax(ajaxConfig)
            .done(r => {
                fieldError.clear();
                response = r;
                btnLoading();
                callback();

                //Reset back to false
                contentType = false;
            })
            .fail(r => {
                if (typeof r.errors !== "undefined") {
                    response = r;
                } else {
                    response = r.responseJSON;
                }
                fieldError.show();
                btnLoading();
                callback();

                //Reset back to false
                contentType = false;
            });
    };

    const btnLoading = () => {
        if (btn.hasClass("loading")) {
            $("body").removeClass("loading");
            btn.removeClass("loading");
        } else {
            $("body").addClass("loading");
            btn.addClass("loading");
        }
    };

    const initPreventDefault = () => {
        $("form").submit(function(e) {
            e.preventDefault();
            let formButton = $(this).find("[data-action]");
            // Click last button instead
            if (formButton.length) {
                formButton[formButton.length - 1].click();
            }
        });
    };

    initChange();
    initBtn();

    initPreventDefault();
});
