jQuery("document").ready(function($) {
    let container;

    const loading = {
        start: () => {
            container.addClass("loading");
            $("body").addClass("loading");
        },
        done: () => {
            container.removeClass("loading");
            $("body").removeClass("loading");
        }
    };

    const headers = {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        "Content-Type": "application/json",
        Accept: "application/json"
    };
    
    const updateSort = (workout_id, sort) => {
        const url = "/workout/step/sort";

        let body = {
            workout_id: workout_id,
            sort: sort
        };

        loading.start();

        fetch(url, {
            method: "POST",
            headers: headers,
            body: JSON.stringify(body)
        })
            .then(response => response.json())
            .then(json => {
                loading.done();
            });
    };

    const initSortable = () => {
        if ($(".sortable").length) {
            sortable(".sortable", {
                items: ".card",
                placeholder: "<hr>"
            });
            sortable(".sortable")[0].addEventListener("sortupdate", function(
                e
            ) {
                container = $(e.detail.origin.container);
                let items = e.detail.origin.items;
                let workout_id = container.data("workout_id");
                let sort = [];
                $.each(items, function(i, v) {
                    let id = $(v).data("id");
                    sort.push({
                        id: id,
                        sort: i
                    });
                });
                updateSort(workout_id, sort);

                /*
            This event is triggered when the user stopped sorting and the DOM position has changed.

            e.detail.item - {HTMLElement} dragged element

            Origin Container Data
            e.detail.origin.index - {Integer} Index of the element within Sortable Items Only
            e.detail.origin.elementIndex - {Integer} Index of the element in all elements in the Sortable Container
            e.detail.origin.container - {HTMLElement} Sortable Container that element was moved out of (or copied from)
            e.detail.origin.itemsBeforeUpdate - {Array} Sortable Items before the move
            e.detail.origin.items - {Array} Sortable Items after the move

            Destination Container Data
            e.detail.destination.index - {Integer} Index of the element within Sortable Items Only
            e.detail.destination.elementIndex - {Integer} Index of the element in all elements in the Sortable Container
            e.detail.destination.container - {HTMLElement} Sortable Container that element was moved out of (or copied from)
            e.detail.destination.itemsBeforeUpdate - {Array} Sortable Items before the move
            e.detail.destination.items - {Array} Sortable Items after the move
            */
            });
        }
    };

    initSortable();
});
