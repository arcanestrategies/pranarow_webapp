# Pranarow Web App

## Info
Core: Based on Laravel 5.8

## Requirements

PHP >= 7.1.3
OpenSSL PHP Extension
PDO PHP Extension
Mbstring PHP Extension
Tokenizer PHP Extension
XML PHP Extension
Ctype PHP Extension
JSON PHP Extension
BCMath PHP Extension

MYSQL >= 5 || MARIA DB >= 10

composer >= 1.65
npm >= 6.4.1

## Setup

use .env.example file to create your own

composer install
php artisan passport:install
npm install
php artisan storage:link
php artisan migrate
php artisan db:seed --class=SetupSeeder
php artisan passport:client --personal

PranaRow

## Development

npm run watch

### Refresh 
php artisan migrate:refresh && php artisan db:seed --class=SetupSeeder && php artisan passport:client --personal && php artisan db:seed --class=DemoSeeder