<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkoutLevel extends Model
{
    public function getCardClassAttribute()
    {
        switch ($this->id) {
            case '1':
                return 'bg-light';
                break;
            case '2':
                return 'text-white bg-info';
                break;
            case '3':
                return 'text-white bg-primary';
                break;
            
            default:
                # code...
                break;
        }
    }
}
