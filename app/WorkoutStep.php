<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkoutStep extends Model
{
    protected $appends = [];

    public function level()
    {
        return $this->hasOne('App\WorkoutLevel','id','level_id');
    }

    public function unit()
    {
        return $this->belongsTo('App\Unit');
    }

    
}
