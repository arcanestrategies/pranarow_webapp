<?php
namespace App\Helpers;

class R extends Helper
{
    public $status;
    public $message;
    public $view;
    public $data;
    public $errors;

    public function __construct(
        $status = false,
        $message = '',
        $view = '',
        $data = [],
        $errors = []
    ) {
        $this->status = $status;
        $this->message = $message;
        $this->view = $view;
        $this->data = $data;
        $this->errors = $errors;
    }
}
