<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Auth;

class Nav
{

    public static function getFooterAboutMenu()
    {
        return [
            [
                'url' => url('about'),
                'title' => __('About Us'),
            ],
            [
                'url' => url('contact'),
                'title' => __('Contact Us'),
            ],
            [
                'url' => url('terms'),
                'title' => __('Terms & Conditions'),
            ],
            [
                'url' => url('privacy'),
                'title' => __('Privacy Policy'),
            ],
        ];
    }
    public static function getFooterShoppingMenu()
    {
        return [
            [
                'url' => url('cart'),
                'title' => __('Cart'),
            ],
            [
                'url' => url('search'),
                'title' => __('Search Product'),
            ],
        ];
    }
    public static function getFooterUserMenu()
    {
        return [
            [
                'url' => url('login'),
                'title' => __('Login'),
            ],
            [
                'url' => url('register'),
                'title' => __('Sign Up'),
            ],
            [
                'url' => url('profile'),
                'title' => __('Profile'),
            ],
        ];
    }
    public static function getSocialMenu()
    {
        return [
            [
                'icon' => 'fab fa-3x fa-facebook',
                'url' => '#',
                'title' => __('Facebook'),
            ],
            [
                'icon' => 'fab fa-3x fa-instagram',
                'url' => '#',
                'title' => __('Instagram'),
            ],
            [
                'icon' => 'fab  fa-3x fa-twitter',
                'url' => '#',
                'title' => __('Twitter'),
            ],
            [
                'icon' => 'fab  fa-3x fa-youtube',
                'url' => '#',
                'title' => __('Youtube'),
            ],
        ];
    }
    public static function getTopMenu()
    {
        $menu = [
            [
                'icon' => '',
                'url' => 'contact',
                'title' => __('Contact'),
            ],
            [
                'icon' => '',
                'url' => 'pricing',
                'title' => __('Pricing'),
            ],
            [
                'icon' => '',
                'url' => 'faq',
                'title' => __('FAQ'),
            ],
        ];

        if (Auth::user()) {
            $menu = array_merge(self::getUserMenu(), $menu);
        }

        return $menu;
    }

    public static function getUserMenu()
    {
        return [
            [
                'icon' => '',
                'url' => 'workouts',
                'title' => __('Workouts'),
                'children' => [
                    [
                        'icon' => '',
                        'url' => '/workout/item',
                        'title' => __('Create Workout'),
                    ],
                ],
            ],
            [
                'icon' => '',
                'url' => 'groups',
                'title' => __('Groups'),
                'children' => [
                    [
                        'icon' => '',
                        'url' => '/group/item',
                        'title' => __('Create Group'),
                    ],
                ],
            ],
        ];
    }
}
