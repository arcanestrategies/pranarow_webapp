<?php
namespace App\Helpers;

class Helper
{

    public function toArray()
    {
        return (array) $this;
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }
}