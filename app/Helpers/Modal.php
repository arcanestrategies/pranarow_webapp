<?php
namespace App\Helpers;

class Modal extends Helper
{
    public $id;
    public $title;
    public $view;
    public $data;

    public function __construct(
        $id = 'modal',
        $title = 'Modal', 
        $view = '', 
        $data = []
        ) {
        $this->id = $id;
        $this->title = $title;
        $this->view = $view;
        $this->data = $data;
    }
}