<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use App\Media;
use App\Profile;
use App\Helpers\R;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * User Information by Username
     *
     * @group User Management
     * @param String $username
     * @return View
     */
    public function index($username = null)
    {
        if ($username) {
            $item = User::where('username', $username)->first();
        } else {
            $item = Auth::user();
        }

        return view('profile.index')
            ->withTitle($item->full_name . ' Profile')
            ->withItem($item);
    }

    public function upgrade(Request $request)
    {
        $user = Auth::user();
        $user->role_id = 2;
        $user->save();
        return response()->json(new R(true, 'User upgraded to Coach'));
    }
    public function degrade(Request $request)
    {
        $user = Auth::user();
        $user->role_id = 1;
        $user->save();
        return response()->json(new R(true, 'User degraded to Basic User'));
    }

    public function search(Request $request)
    {
        $request->validate([
            'search' => 'required|min:3',
        ]);

        $search = $request->search;
        $search_array = explode(' ', $request->search);

        $per_page = $request->per_page ?? 10;
        $current_page = $request->current_page ?? 1;

        $users = User::search($request->search);

        if ($users->exists()) {
            $users = $users->paginate($per_page);
            $view = view('modules.user.selectlist')->withUsers($users)->render();
            return response()->json(new R(true, 'Users found', $view, $users));
        }
        return response()->json(new R(false, 'Users not for search: ' . $request->search));
    }

    public function edit()
    {
        $item = Auth::user();
        return view('profile.edit')
            ->withTitle('Updating ' . $item->full_name . ' Profile')
            ->withItem($item);
    }

    /**
     * User Information
     * Returns logged in user if {username} is not provided
     *
     * @authenticated
     * @group User Management
     * @return R
     */
    public function user($username = null)
    {
        if ($username) {
            $item = User::where('username', $username)->first();
        } else {
            $item = Auth::user();
        }
        $r = new R(true, 'User information', null, $item);
        return $r->toArray();
    }

    public function store(Request $request)
    {
        $user = Auth::user();
        $user_id = $user->id;
        $request->validate([
            'username' => 'required|unique:users,username,' . $user_id,
            'email' => 'required|unique:users,email,' . $user_id,
        ]);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->phone = $request->phone;
        $user->pin = $request->pin;
        $user->save();

        $profile = $user->profile;
        if (!$profile) {
            $profile = new Profile;
            $profile->user_id = $user_id;
        }
        $profile->birth_date = $request->birth_date;
        $profile->save();

        if ($request->has('image')) {
            $media = Media::createOrUpdate($request, 'image', $user->id, 'user');
        }

        $r = new R(true, __('Profile Updated'));
        return $r->toArray();
    }
}
