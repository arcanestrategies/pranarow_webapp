<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\R;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'profile';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * User Register
     *
     * @bodyParam username string required Username
     * @bodyParam email email required Email Address
     * @bodyParam password password required Password
     * @bodyParam password_confirmation password required Password Confirmation
     * @group User Management
     * @param Request $request
     * @return void
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user)
        ?: redirect($this->redirectPath());
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => ['required', 'string', 'max:255', 'unique:users'],
            // 'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $first_name = null;
        $last_name = null;
        if (isset($data['name'])) {
            $name_array = explode(' ', $data['name']);
            if (sizeof($name_array) > 1) {
                $first_name = end($name_array);
                $last_name = end($name_array);
            } else {
                $first_name = $data['name'];
                $last_name = '';
            }
        }
        return User::create([
            'first_name' => $first_name,
            'last_name' => $last_name,
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'token' => Str::random(32),
            'role_id' => 1,
            'status' => 1,
        ]);
    }

    protected function registered(Request $request, $user)
    {
        $r = new R(true, __('Thank you for registering, Redirecting to your Dashboard'));
        $r->url = $this->redirectTo;
        $token = $user->createToken('PranaRow')->accessToken;
        $r->token = $token;
        return $r->toArray();
    }
}
