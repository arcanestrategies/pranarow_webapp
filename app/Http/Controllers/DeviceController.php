<?php

namespace App\Http\Controllers;

use App\Device;
use App\Helpers\R;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DeviceController extends Controller
{
    public function remove(Request $request)
    {
        $device = Device::find($request->device_id);
        if(!$device){
            return response()->json(new R(false, __('Device not found')));
        }
        if($device->user_id != Auth::user()->id){
            return response()->json(new R(false, __('Access Denied')));
        }
        $device->delete();
        return response()->json(new R(true, __('Device Removed')));
    }

    /**
     * Register device
     * 
     * @group Device Management
     * @bodyParam serial string required Serial Number of Device
     * @bodyParam name string required Device Name
     * @param Request $request
     * @return void
     */
    public function register(Request $request)
    {
        $user = Auth::user();

        $request->validate([
            'serial' => 'required',
            'name' => 'required',
        ]);

        $device = Device::where('serial', $request->serial)->first();
        $message = __('Device Re-Registered to new user');
        if (!$device) {
            $device = new Device;
            $message = __('Device Registered');
        }
        $device->user_id = $user->id;
        $device->serial = $request->serial;
        $device->name = $request->name;
        $device->save();

        return response()->json(new R(true, $message));
    }

    /**
     * List Devices
     *
     * @group Device Management
     * @return void
     */
    public function list()
    {
        $user = Auth::user();
        $devices = Device::where('user_id', $user->id)->get();
        return response()->json(new R(true, __('Device List'),null,$devices));
    }
}
