<?php

namespace App\Http\Controllers;

use App\User;
use App\Group;
use App\Media;
use App\Workout;
use App\Helpers\R;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WorkoutController extends Controller
{
    private $workout;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function fixUsers()
    {
        $workouts = Workout::get();
        foreach ($workouts as $workout) {
            $workout->users()->syncWithoutDetaching([$workout->user_id => ['access' => 9]]);
        }
        dd('done');
    }

    private function validateWorkout(Request $request)
    {
        $r = new R(false);
        $item = Workout::find($request->workout_id);

        if ($item) {
            if ($item->user_id != Auth::user()->id) {
                $r->message = __('Access Denied');
                return $r;
            }
        } else {
            $r->message = __('Workout Not Found');
            return $r;
        }
        $this->workout = $item;
        $r->status = true;
        return $r;
    }

    public function index(Request $request)
    {
        $user = Auth::user();
        return view('workout.index')->withUser($user)->withTitle(__('Workouts'));
    }

    public function item($id = null)
    {
        $item = null;
        $title = __('Create Workout');
        if ($id) {
            $title = __('Edit Workout');
            $item = Workout::find($id);
            if($item->user_id != Auth::user()->id){
                return abort(403);
            }
        }
        return view('workout.item')->withItem($item)->withTitle($title);
    }

    public function view($id = null)
    {
        $item = Workout::find($id);
        if(!$item){
            return abort(404);
        }
        $title = __('View Workout');
        return view('workout.item')->withItem($item)->withTitle($title)->withReadonly('true');
    }

    public function userAdd(Request $request)
    {
        if (!$request->users) {
            return response()->json(new R(false, __('Select User(s) To Add')));
        }

        $r = $this->validateWorkout($request);
        if ($r->status == false) {
            return response()->json($r);
        }

        $user_ids = $request->users;
        foreach ($user_ids as $user_id) {
            $user = User::find($user_id);
            if ($user) {
                $this->workout->users()->syncWithoutDetaching([$user->id => ['access' => 1]]);
            }
        }

        return response()->json(new R(true, __('Users Added')));
    }

    public function userRemove(Request $request)
    {
        if($request->user_id == Auth::user()->id){
            return response()->json(new R(false, __("Don't remove yourself!")));
        }

        $r = $this->validateWorkout($request);
        if ($r->status == false) {
            return response()->json($r);
        }

        $user = User::find($request->user_id);
        if (!$user) {
            return response()->json(new R(false, __('User Not Found')));
        }
        $this->workout->users()->detach($user->id);

        return response()->json(new R(true, __('User Removed')));
    }

    public function groupAdd(Request $request)
    {
        if (!$request->groups) {
            return response()->json(new R(false, __('Select Group(s) To Add')));
        }

        $r = $this->validateWorkout($request);
        if ($r->status == false) {
            return response()->json($r);
        }

        $group_ids = $request->groups;
        foreach ($group_ids as $group_id) {
            $group = Group::find($group_id);
            if ($group && $group->isMember()) {
                $this->workout->groups()->syncWithoutDetaching($group->id);
            }
        }

        return response()->json(new R(true, __('Group(s) Added')));
    }

    
    public function groupRemove(Request $request)
    {
        $r = $this->validateWorkout($request);
        if ($r->status == false) {
            return response()->json($r);
        }

        $group = Group::find($request->group_id);
        if (!$group) {
            return response()->json(new R(false, __('Group Not Found')));
        }
        $this->workout->groups()->detach($group->id);

        return response()->json(new R(true, __('Group Removed')));
    }

    /**
     * List Workout
     *
     * @group Workout Management
     * @param Request $request
     * @return R
     */
    function list(Request $request) {
        return response()->json(new R(true, __('Workouts'), null, Auth::user()->workouts));
    }

    /**
     * Single Workout
     *
     * @group Workout Management
     * @param Request $request
     * @return R
     */
    function itemApi($id) {
        $item = Workout::find($id);
        if ($item) {
            if ($item->user_id != Auth::user()->id) {
                return response()->json(new R(false, __('Access Denied')));
            }
        } else {
            return response()->json(new R(false, __('Workout not found')));
        }
        return response()->json(new R(true, __('Workout'), null, $item));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $r = new R;
        $r->message = __('Workout Created');
        $user = Auth::user();
        $item = Workout::find($request->id);

        if ($item) {
            if ($item->user_id != $user->id) {
                $r->status = false;
                $r->message = __('Access Denied');
                return $r->toArray();
            }
            $r->message = __('Workout Updated');
        } else {
            $item = new Workout;
        }
        $item->user_id = $user->id;
        $item->focus_id = $request->focus_id;
        $item->name = $request->name;
        $item->description = $request->description;
        $item->save();

        if ($request->has('image')) {
            $media = Media::createOrUpdate($request, 'image', $item->id, 'workout');
        }

        $item->users()->syncWithoutDetaching([$user->id => ['access' => 9]]);
        $r->status = true;
        $r->url = $item->url;
        return $r->toArray();

    }
}
