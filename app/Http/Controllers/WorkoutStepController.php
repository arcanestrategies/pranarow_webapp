<?php

namespace App\Http\Controllers;

use App\Helpers\R;
use App\Workout;
use App\WorkoutStep;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WorkoutStepController extends Controller
{
    private $workout;
    private $step;

    public function validateStep(Request $request)
    {
        $r = new R;
        $step = WorkoutStep::find($request->step_id);
        if (!$step) {
            $r->status = false;
            $r->message = __('Workout Step Missing');
            return $r->toArray();
        }
        $this->step = $step;
        $r->status = true;
        return $r->toArray();
    }

    public function validateWorkout(Request $request)
    {
        $r = new R;
        $workout = Workout::find($request->workout_id);
        $user = Auth::user();
        if ($workout) {
            if ($workout->user_id != $user->id) {
                $r->status = false;
                $r->message = __('Access Denied');
                return $r->toArray();
            }
        } else {
            $r->status = false;
            $r->message = __('Workout Missing');
            return $r->toArray();
        }
        $this->workout = $workout;
        $r->status = true;
        return $r->toArray();
    }

    public function remove(Request $request)
    {
        $validate = $this->validateWorkout($request);
        if ($validate['status'] == false) {
            return $validate;
        }

        $validate_step = $this->validateStep($request);
        if ($validate_step['status'] == false) {
            return $validate_step;
        }

        $this->step->delete();
        return response()->json(new R(true, __('Step Removed')));
    }

    public function store(Request $request)
    {
        $request->validate([
            'level_id' => 'required',
            'duration' => 'required',
        ]);

        $r = new R;
        $r->message = __('Workout Step Created');

        $validate = $this->validateWorkout($request);
        if ($validate['status'] == false) {
            return $validate;
        }

        $step = WorkoutStep::find($request->id);
        if (!$step) {
            $step = new WorkoutStep;
        }

        $step->workout_id = $request->workout_id;
        $step->level_id = $request->level_id;
        $step->unit_id = $request->unit_id ?? 1;
        $step->sort = $request->sort ?? 1;
        $step->name = $request->name;
        $step->duration = $request->duration;
        $step->distance = $request->distance;
        $step->save();

        $r->status = true;
        $r->view = view('workoutstep.steps')->withItem($this->workout)->render();
        return $r->toArray();
    }

    public function sort(Request $request)
    {
        $validate = $this->validateWorkout($request);
        if ($validate['status'] == false) {
            return $validate;
        }
        foreach ($request->sort as $sort) {
            $step = WorkoutStep::find($sort['id']);
            $step->sort = $sort['sort'];
            $step->save();
        }
        return response()->json(new R(true, 'Sort Updated'));
    }
}
