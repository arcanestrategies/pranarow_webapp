<?php

namespace App\Http\Controllers\Api;

use App\Helpers\R;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Login User
     *
     * @response 422 {
     * "message": "The given data was invalid.",
     * "errors": {
     * "email": [
     *  "The email field is required."
     * ],
     * "password": [
     * "The password field is required."
     * ]
     * }
     * }
     *
     * @response {
     * "status": true,
     * "view": "",
     * "message": "Logged In",
     * "errors": [],
     * "data": [],
     * "access_token": "ACCESS TOKEN"
     * }
     * @bodyParam email string required User email
     * @bodyParam password string required User email
     * @group User Management
     * @param Request $request
     * @return R
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean',
        ]);
        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials)) {
            return response()->json([
                'message' => 'Unauthorized',
            ], 401);
        }

        $user = $request->user();

        $tokenResult = $user->createToken(config('app.name'));
        $token = $tokenResult->token;
        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }

        $token->save();

        $r = new R(true, 'Logged In');
        $r->access_token = $tokenResult->accessToken;
        return $r->toArray();

    }

    /**
     * Logout User
     *
     * @group User Management
     * @return R message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        $r = new R(true, 'Logged Out');
        return $r->toArray();
    }
}
