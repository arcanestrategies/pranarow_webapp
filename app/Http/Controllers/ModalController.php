<?php

namespace App\Http\Controllers;

use App\Helpers\Modal;
use App\Helpers\R;
use Illuminate\Http\Request;

class ModalController extends Controller
{
    public function create(Request $request)
    {
        $modal = new Modal($request->id, $request->title, $request->view, $request->data);
        $view = view('modules.modal')->withModal($modal)->render();
        
        $r = new R(true, 'Loading', $view);
        return $r->toArray();
    }
}
