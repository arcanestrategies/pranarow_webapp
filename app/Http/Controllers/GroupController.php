<?php

namespace App\Http\Controllers;

use App\User;
use App\Group;
use App\Media;
use App\Helpers\R;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GroupController extends Controller
{
    private $group;

    public function index(Request $request)
    {
        $user = Auth::user();
        return view('group.index')->withUser($user)->withTitle(__('Groups'));
    }

    private function validateGroup(Request $request)
    {
        $r = new R(false);
        $item = Group::find($request->group_id);

        if ($item) {
            if (!$item->isOwner()) {
                $r->message = __('Access Denied');
                return $r;
            }
        } else {
            $r->message = __('Group Not Found');
            return $r;
        }
        $this->group = $item;
        $r->status = true;
        return $r;
    }

    public function userPromote(Request $request)
    {
        $r = $this->validateGroup($request);
        if ($r->status == false) {
            return response()->json($r);
        }

        $user = User::find($request->user_id);
        if (!$user) {
            return response()->json(new R(false, __('User Not Found')));
        }

        $this->group->users()->updateExistingPivot($request->user_id, ['access' => 9]);

        $view = view('modules.user.item')->withUser($user)->withItem($this->group)->render();
        return response()->json(new R(true, __('User Promoted to Owner'), $view));
    }

    public function userDemote(Request $request)
    {
        $r = $this->validateGroup($request);
        if ($r->status == false) {
            return response()->json($r);
        }

        $user = User::find($request->user_id);
        if (!$user) {
            return response()->json(new R(false, __('User Not Found')));
        }

        $this->group->users()->updateExistingPivot($request->user_id, ['access' => 1]);

        $view = view('modules.user.item')->withUser($user)->withItem($this->group)->render();
        return response()->json(new R(true, __('User Demoted to Member'), $view));
    }

    public function userRemove(Request $request)
    {

        $r = $this->validateGroup($request);
        if ($r->status == false) {
            return response()->json($r);
        }

        $user = User::find($request->user_id);
        if (!$user) {
            return response()->json(new R(false, __('User Not Found')));
        }
        $this->group->users()->detach($request->user_id);

        return response()->json(new R(true, __('User Removed from Group')));
    }

    public function userLeave(Request $request)
    {
        $group = Group::find($request->group_id);
        if(!$group){
            response()->json(new R(false, __('Group Not Found')));
        }

        $user = Auth::user();
        $group->users()->detach($user->id);

        return response()->json(new R(true, __('Leaving Group')));
    }

    public function userAdd(Request $request)
    {
        if (!$request->users) {
            return response()->json(new R(false, __('Select Users To Add')));
        }

        $r = $this->validateGroup($request);
        if ($r->status == false) {
            return response()->json($r);
        }

        $user_ids = $request->users;
        foreach ($user_ids as $user_id) {
            $user = User::find($user_id);
            if ($user) {
                $this->group->users()->syncWithoutDetaching([$user->id => ['access' => 1]]);
            }
        }

        return response()->json(new R(true, __('Users Added')));
    }

    public function item($id = null)
    {
        $item = null;
        $title = __('Create Group');
        if ($id) {
            $title = __('Edit Group');
            $item = Group::find($id);
            if(!$item->isOwner(Auth::user()->id)){
                return abort(403);
            }
        }
        return view('group.item')->withItem($item)->withTitle($title);
    }

    public function view($id = null)
    {
        $title = __('View Group');
        $item = Group::find($id);
        if(!$item){
            return abort(404);
        }
        return view('group.item')->withItem($item)->withTitle($title)->withReadonly(true);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $r = new R;
        $r->message = __('Group Created');
        $user = Auth::user();
        $item = Group::find($request->id);

        if ($item) {
            if (!$item->isOwner()) {
                return response()->json(new R(false, __('Access Denied')));
            }
            $r->message = __('Group Updated');
        } else {
            $item = new Group;
        }
        $item->name = $request->name;
        $item->description = $request->description;
        $item->save();

        if ($request->has('image')) {
            $media = Media::createOrUpdate($request, 'image', $item->id, 'group');
        }
        $item->users()->syncWithoutDetaching([$user->id => ['access' => 9]]);
        $r->status = true;
        $r->url = $item->url;
        return $r->toArray();

    }
}
