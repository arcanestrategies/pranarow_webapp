<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $dates= ['birth_date'];

    public function getBirthDateAttribute($value)
    {
        return Carbon::parse($value)->toDateString();
    }
}
