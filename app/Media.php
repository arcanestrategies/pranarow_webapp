<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = 'medias';

    public static function createOrUpdate($request,$field,$item_id,$item_type)
    {
        $media = self::where('item_id',$item_id)->where('item_type',$item_type)->first();
        if(!$media){
            $media = new self;
            $media->item_id = $item_id;
            $media->item_type = $item_type;
            $media->media_type = 'image';
        }

        $path = $request->file($field)->store($item_type,'public');

        $media->path = $path;
        $media->save();
        
        return $media;
    }
}
