<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Group extends Model
{
    public function users()
    {
        return $this->belongsToMany('App\User', 'group_user', 'group_id', 'user_id')->withPivot('access');
    }

    public function workouts()
    {
        return $this->belongsToMany('App\Workout', 'workout_group', 'group_id', 'workout_id');
    }

    public function image()
    {
        return $this->hasOne('App\Media', 'item_id', 'id')->where('item_type', 'group');
    }

    public function getImagePathAttribute($value)
    {
        if ($this->image) {
            return asset('storage/'.$this->image->path);
        }
        return asset('img/avatar.png');
    }

    public function getUrlAttribute()
    {
        return url('group/item/' . $this->id);
    }

    public function getViewUrlAttribute()
    {
        return url('group/view/' . $this->id);
    }

    public function getOwnerListAttribute()
    {
        return $this->users()->wherePivot('access', 9)->pluck('user_id')->toArray();
    }

    public function getMemberListAttribute()
    {
        return $this->users()->pluck('user_id')->toArray();
    }

    public function isMember($user_id = null)
    {
        if (!$user_id) {
            $user_id = Auth::user()->id;
        }
        return (in_array($user_id, $this->member_list));
    }

    public function isOwner($user_id = null)
    {
        if (!$user_id) {
            $user_id = Auth::user()->id;
        }
        if ($this->isCoach($user_id)){
            return true;
        }
        return in_array($user_id, $this->owner_list);
    }

    public function isCoach($user_id = null)
    {
        if (!$user_id) {
            $user_id = Auth::user()->id;
        }
        $user = User::find($user_id);
        return ($user && $this->isMember($user_id) && $user->role_id == 2);
    }

    public function getTotalMembersAttribute()
    {
        return $this->users->count();
    }
}
