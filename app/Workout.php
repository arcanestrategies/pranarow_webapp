<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

class Workout extends Model
{

    protected $appends = [
        'total_duration',
        'total_steps',
        'image_path',
    ];

    public function image()
    {
        return $this->hasOne('App\Media', 'item_id', 'id')->where('item_type', 'workout');
    }

    public function getImagePathAttribute($value)
    {
        if ($this->image) {
            return asset('storage/'.$this->image->path);
        }
        return asset('img/avatar.png');
    }

    public function getUrlAttribute()
    {
        return url('workout/item/' . $this->id);
    }

    public function getViewUrlAttribute()
    {
        return url('workout/view/' . $this->id);
    }

    public function steps()
    {
        return $this->hasMany('App\WorkoutStep')->orderBy('sort', 'ASC');
    }

    public function getTotalDurationAttribute()
    {
        $total = 0;
        if ($this->steps) {
            foreach ($this->steps as $step) {
                $total += $step->duration;
            }
        }
        return $total;
    }

    public function getTotalStepsAttribute()
    {
        $total = 0;
        if ($this->steps) {
            return $this->steps->count();
        }
        return $total;
    }

    public function getOwnerListAttribute()
    {
        return $this->users()->wherePivot('access',9)->pluck('user_id')->toArray();
    }

    public function isOwner($user_id = null)
    {
        if(!$user_id){
            $user_id = Auth::user()->id;
        }
        return in_array($user_id,$this->ownerList);
    }

    public function users()
    {
        return $this->belongsToMany('App\User', 'workout_user', 'workout_id', 'user_id')->withPivot('access');
    }

    public function groups()
    {
        return $this->belongsToMany('App\Group', 'workout_group', 'workout_id', 'group_id');
    }
}
