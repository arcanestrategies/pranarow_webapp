<?php

namespace App;

use App\Traits\FullTextSearch;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, FullTextSearch;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'first_name',
        'last_name',
        'email',
        'password',
        'status',
        'role_id',
        'token',
    ];

    /**
     * The columns of the full text index
     */
    protected $searchable = [
        'username',
        'first_name',
        'last_name',
        'email',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'token', 'pin',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function image()
    {
        return $this->hasOne('App\Media', 'item_id', 'id')->where('item_type', 'user');
    }

    public function getImagePathAttribute()
    {
        if ($this->image) {
            return asset('storage/' . $this->image->path);
        }
        return asset('img/avatar.png');
    }

    public function profile()
    {
        return $this->hasOne('App\Profile');
    }

    public function getFullNameAttribute()
    {
        $full_name = $this->first_name . ' ' . $this->last_name;
        if (!empty(str_replace(' ', '', $full_name))) {
            return $full_name;
        }
        return $this->username;
    }

    public function getGroupWorkoutsAttribute()
    {
        $workouts = null;
        foreach ($this->groups as $group) {
            if ($group) {
                $current_workouts = $group->workouts()->get();
                if ($workouts) {
                    $workouts = $workouts->merge($current_workouts);
                } else {
                    $workouts = $current_workouts;
                }
            }
        }
        return $workouts;
    }

    public function shared_workouts()
    {
        return $this->belongsToMany('App\Workout', 'workout_user', 'user_id', 'workout_id')->withPivot('access');
    }

    public function getWorkoutsAttribute()
    {
        $workouts = null;
        $shared_workouts = $this->shared_workouts()->get();
        $group_workouts = $this->group_workouts;
        if (!empty($shared_workouts) && !empty($group_workouts)) {
            $workouts = $shared_workouts->merge($group_workouts);
        } else if (!empty($shared_workouts)) {
            return $shared_workouts;
        } else if (!empty($group_workouts)) {
            return $group_workouts;
        }
        return $workouts;
    }

    public function groups()
    {
        return $this->belongsToMany('App\Group');
    }

    public function getProfileUrlAttribute()
    {
        return url('profile/view/' . $this->username);
    }

    public function devices()
    {
        return $this->hasMany('App\Device');
    }

}
