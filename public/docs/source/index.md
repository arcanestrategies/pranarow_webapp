---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#Device Management
<!-- START_8f41217bf023ddef5d8995d7c7c7e2e2 -->
## List Devices

> Example request:

```bash
curl -X GET -G "http://localhost/api/devices" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```
```javascript
const url = new URL("http://localhost/api/devices");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/devices`


<!-- END_8f41217bf023ddef5d8995d7c7c7e2e2 -->

<!-- START_42b21542717b416cecfe56ef68834eae -->
## Register device

> Example request:

```bash
curl -X POST "http://localhost/api/device/register" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}" \
    -d '{"serial":"inventore","name":"cum"}'

```
```javascript
const url = new URL("http://localhost/api/device/register");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
}

let body = {
    "serial": "inventore",
    "name": "cum"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`POST api/device/register`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    serial | string |  required  | Serial Number of Device
    name | string |  required  | Device Name

<!-- END_42b21542717b416cecfe56ef68834eae -->

#User Management
<!-- START_2e1c96dcffcfe7e0eb58d6408f1d619e -->
## User Register

> Example request:

```bash
curl -X POST "http://localhost/api/auth/register" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"username":"quas","email":"velit","password":"saepe","password_confirmation":"voluptatum"}'

```
```javascript
const url = new URL("http://localhost/api/auth/register");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "username": "quas",
    "email": "velit",
    "password": "saepe",
    "password_confirmation": "voluptatum"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "status": true,
    "message": "Thank you for registering, Redirecting to your Dashboard",
    "view": "",
    "data": [],
    "errors": [],
    "url": "profile",
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjcyNTQ5ODhkMjY0MzdmZjBkNTNjNDQ0N2E1NDVjNjQ0NWM4NWFkNDc1ZDMzNjM4ZmY2MzIzMjE5ODRjOTc0Y2M1ODk1ZGNlMGI3NDAxMGUwIn0.eyJhdWQiOiIxIiwianRpIjoiNzI1NDk4OGQyNjQzN2ZmMGQ1M2M0NDQ3YTU0NWM2NDQ1Yzg1YWQ0NzVkMzM2MzhmZjYzMjMyMTk4NGM5NzRjYzU4OTVkY2UwYjc0MDEwZTAiLCJpYXQiOjE1NTkwNTM1NDMsIm5iZiI6MTU1OTA1MzU0MywiZXhwIjoxNTkwNjc1OTQzLCJzdWIiOiIyNTgiLCJzY29wZXMiOltdfQ.UQyg7bmFTa4t_ZdkVC7logJe50YE9iHoo_ePadUQYXUFy-Ncl9wF_fPWvMkESfIRzi-m1KOKSjU7lBeAaMG14JC2mj7Ptb7zSWAOUuoJJkpL4sa14dbjuQ79gsEkpwMq_zFhHxpOG0IdwLZAw-VFJbHTOSnHe_L43LQs2QGrNJ87dUH3TzjUOCifJWx6zmnWS7N_Hbxw3jlyk4gXsrm8v5VZnmOrrXcRPq09JL-17jg2tIzjYKOAVRbJx5Od98erRGpQdG65zNvAmkhMTDx-Poif28OZVIjbuhZOIe0xdCe_ACunEuNN8-_UJGm0jSMszAm7HuMJgJiefGOGtUEDT93xlSSqH1lE8CgGyVTjk2rpriU7ARO2R87LjnSr7Qpf0HEmCll9hOVfW3eEnz_gnck7EfyiKJZ5kcly8ODR8KZoW579YJwPeU7g9CfwO1K3cxs3WciH35nXvYr5UqJAGaobV5ND8SKGbpzmy_YJ-l9XaPoIJn-vJIAOIXlSXNItX6D9caTAqi_WGuaK1-iCpbktt-eLClOCeiwuKR_77QebC53Aeo7GTk-fpO7Zk6F0oyRBb6Ix0mdO2QJUdy9DZ7mmDj0rJMwldi9ywW5uPV1bO9ZabbdxkUmrRGTjz5xs2zu4dN6dC7Z-K_QbskeycxQQs08zNdWz90wYclvX8oU"
}
```

### HTTP Request
`POST api/auth/register`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    username | string |  required  | Username
    email | email |  required  | Email Address
    password | password |  required  | Password
    password_confirmation | password |  required  | Password Confirmation

<!-- END_2e1c96dcffcfe7e0eb58d6408f1d619e -->

<!-- START_a925a8d22b3615f12fca79456d286859 -->
## Login User

> Example request:

```bash
curl -X POST "http://localhost/api/auth/login" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"email":"ad","password":"voluptate"}'

```
```javascript
const url = new URL("http://localhost/api/auth/login");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "email": "ad",
    "password": "voluptate"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (422):

```json
{
    "message": "The given data was invalid.",
    "errors": {
        "email": [
            "The email field is required."
        ],
        "password": [
            "The password field is required."
        ]
    }
}
```
> Example response (200):

```json
{
    "status": true,
    "view": "",
    "message": "Logged In",
    "errors": [],
    "data": [],
    "access_token": "ACCESS TOKEN"
}
```

### HTTP Request
`POST api/auth/login`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    email | string |  required  | User email
    password | string |  required  | User email

<!-- END_a925a8d22b3615f12fca79456d286859 -->

<!-- START_16928cb8fc6adf2d9bb675d62a2095c5 -->
## Logout User

> Example request:

```bash
curl -X GET -G "http://localhost/api/auth/logout" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```
```javascript
const url = new URL("http://localhost/api/auth/logout");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/auth/logout`


<!-- END_16928cb8fc6adf2d9bb675d62a2095c5 -->

<!-- START_ff6d656b6d81a61adda963b8702034d2 -->
## User Information
Returns logged in user if {username} is not provided

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET -G "http://localhost/api/auth/user" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```
```javascript
const url = new URL("http://localhost/api/auth/user");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/auth/user`


<!-- END_ff6d656b6d81a61adda963b8702034d2 -->

<!-- START_c7d85cc7d5fae30749a6d26a189b23d9 -->
## User Information
Returns logged in user if {username} is not provided

<br><small style="padding: 1px 9px 2px;font-weight: bold;white-space: nowrap;color: #ffffff;-webkit-border-radius: 9px;-moz-border-radius: 9px;border-radius: 9px;background-color: #3a87ad;">Requires authentication</small>
> Example request:

```bash
curl -X GET -G "http://localhost/api/auth/user/sor" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```
```javascript
const url = new URL("http://localhost/api/auth/user/sor");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/auth/user/{username}`


<!-- END_c7d85cc7d5fae30749a6d26a189b23d9 -->

#Workout Management
<!-- START_e7acd3b087558659975d760f84725748 -->
## List Workout

> Example request:

```bash
curl -X GET -G "http://localhost/api/workouts" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```
```javascript
const url = new URL("http://localhost/api/workouts");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/workouts`


<!-- END_e7acd3b087558659975d760f84725748 -->

<!-- START_dd1a0707d0a0c7193887b6a9698b04c2 -->
## Single Workout

> Example request:

```bash
curl -X GET -G "http://localhost/api/workout/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer {token}"
```
```javascript
const url = new URL("http://localhost/api/workout/1");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer {token}",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (401):

```json
{
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/workout/{id}`


<!-- END_dd1a0707d0a0c7193887b6a9698b04c2 -->


