<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::get('/', 'PageController@home');

Auth::routes();

Route::get('profile/view/{username?}', 'ProfileController@index');
Route::post('profile/search', 'ProfileController@search');

Route::post('modal/create', 'ModalController@create');

Route::group(['middleware' => ['auth']], function () {

    // Profile Routes
    Route::group(['prefix' => 'profile'], function () {
        Route::get('/', 'ProfileController@index');
        Route::get('edit', 'ProfileController@edit');
        Route::post('store', 'ProfileController@store');
        Route::post('upgrade', 'ProfileController@upgrade');
        Route::post('degrade', 'ProfileController@degrade');
    });

    // Workout Routes
    Route::get('workouts', 'WorkoutController@index');
    Route::get('workouts/fixusers', 'WorkoutController@fixUsers');
    Route::group(['prefix' => 'workout'], function () {
        Route::get('item/{id?}', 'WorkoutController@item');
        Route::get('view/{id}', 'WorkoutController@view');
        Route::post('store', 'WorkoutController@store');

        Route::group(['prefix' => 'step'], function () {
            Route::post('store', 'WorkoutStepController@store');
            Route::post('remove', 'WorkoutStepController@remove');
            Route::post('sort', 'WorkoutStepController@sort');
        });

        Route::group(['prefix' => 'user'], function () {
            Route::post('add', 'WorkoutController@userAdd');
            Route::post('remove', 'WorkoutController@userRemove');
            Route::post('leave', 'WorkoutController@userLeave');
        });

        Route::post('group/add', 'WorkoutController@groupAdd');
        Route::post('group/remove', 'WorkoutController@groupRemove');
    });

    // Group Routes
    Route::get('groups', 'GroupController@index');
    Route::group(['prefix' => 'group'], function () {
        Route::get('item/{id?}', 'GroupController@item');
        Route::get('view/{id}', 'GroupController@view');
        Route::post('store', 'GroupController@store');

        Route::group(['prefix' => 'user'], function () {
            Route::post('add', 'GroupController@userAdd');
            Route::post('promote', 'GroupController@userPromote');
            Route::post('demote', 'GroupController@userDemote');
            Route::post('remove', 'GroupController@userRemove');
            Route::post('leave', 'GroupController@userLeave');
        });
    });

    Route::post('device/remove', 'DeviceController@remove');

});
