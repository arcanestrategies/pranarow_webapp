<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::group([
    'prefix' => 'auth',
], function () {
    Route::post('register', 'Auth\RegisterController@register');
    Route::post('login', 'Api\AuthController@login');

    Route::group([
        'middleware' => 'auth:api',
    ], function () {
        Route::get('logout', 'Api\AuthController@logout');
        Route::get('user', 'ProfileController@user');
        Route::get('user/{username}', 'ProfileController@user');
    });
});

Route::group([
    'middleware' => 'auth:api',
], function () {
    Route::get('workouts', 'WorkoutController@list');
    Route::get('workout/{id}', 'WorkoutController@itemApi');

    Route::get('devices', 'DeviceController@list');
    Route::post('device/register', 'DeviceController@register');
});
